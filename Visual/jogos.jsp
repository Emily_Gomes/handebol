<%-- 
    Document   : jogos
    Created on : 26/04/2020, 12:54:41
    Author     : emily
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/jogos.css" rel="stylesheet">
         <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
         <link  rel = "icon"  href = "img/logo.ico"  type = "image / x-icon" >
        <title> KLB Sports - Registrar Jogos</title>
    </head>
    <body style="background-color:#ececec">
          <%
		String email = (String) session.getAttribute("Ln1");
                String email2 = (String) session.getAttribute("Ln2");
		if(email2 != null){
			response.sendRedirect("cadjogador.jsp");
		}else if(email == null){
                    response.sendRedirect("login.jsp");
                    }else{
	%>
<nav class="navbar navbar-dark ">
  <a class="navbar-brand" href="index.jsp">
    <img src="img/log.png" width="300" height="70" alt="" href="index.jsp">
  </a>
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Alternar de navegação">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="navbar-collapse collapse" id="navbarsExample01" style="">
    <ul class="navbar-nav mr-auto">
 <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"> Jogos</font></a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Registrar Jogo </font></font></a>
    <a class="dropdown-item" href="cadjogador.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cadastrar Jogadores </font></font></a>
     <a class="dropdown-item" href="partida.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Registrar Dados da Partida </font></font></a>

        </div>
      </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"> Dados</font></a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="TabPartida.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Vizualizar dados da Partida </font></font></a>
    <a class="dropdown-item" href="TabJogo.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vizualizar dados do Jogo </font></font></a>
<a class="dropdown-item" href="dadosj.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vizualizar Jogadores </font></font></a>
        </div>
      </li>
      
          
      
     <li class="nav-item ">
        <a class="nav-link text-white" href="sair.jsp" id="dropdown01"  aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sair</font></font></a>
          </li>
      </li>
    </ul>
  </div>
</nav>
  
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="img/jogos.png" class="d-block w-100" alt="Banner">
    </div>
  </div>
</div>
        <div class="container marketing">
            <div class='row'>
                <div class="col-12"><img src="img/reg.png" class="mx-auto d-block" style="margin-top: -58px;" width="350" height="200"/></div>
            </div>
             <hr class="featurette-divider" style="margin-top:-50px">
        	<div class="d-flex justify-content-center h-100">
        <div class="card">
        <div class="card-header">
        <form action="ServJogo" method="post">
            <h1 style='text-align: center'>Agendamento</h1>
            <br>
            <h4>Local</h4><input type="text" name="local" class="form-control"  placeholder="Digite o nome do local">
            <h4>Data</h4><input type="date" name="data" class="form-control" placeholder="Digite a Data do jogo">
            <h4>Hora</h4><input type="time" name="hora" class="form-control" placeholder="Digite o horário">
            <h4>Nome  do time adversario</h4><input type="text" name="timeAdv"class="form-control"  placeholder="Digite o nome do time adversário">
            <h4>Juiz</h4><input type="text"  name="juiz" class="form-control" placeholder="Nome do Juíz">
            <h4>Secretário</h4><input type="text" name="secretario" class="form-control"  placeholder="Nome do secretário">
            <br>
            <input type="submit" name="enviar" value="Enviar">
            
        </form>
        </div>
        </div></div></div>
        
        
    
        
        
        
        
        
        
        
        
            <nav class="navbar navbar-expand-lg navbar-light ">
           
            <p style="color: white"> Contatos das Desenvolvedoras: </p>
            <a href="https://www.instagram.com/emily___gomes/" style="margin-left: 15px;margin-top: -13px; color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/insta.png" data-holder-rendered="true"> @emily___gomes  </a>
 
            <a href="https://www.instagram.com/b_ssilva74/" style="margin-left: 15px;margin-top: -13px;color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/insta.png" data-holder-rendered="truo d-block"true"> @b_ssilva74 </a>

           
            <a href="https://www.instagram.com/_lleticianasc/" style="margin-left: 15px;margin-top: -13px ;color: white"><img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/insta.png" data-holder-rendered="true">@_lleticianasc </a>

            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px; color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/gmail.png" data-holder-rendered="true"> emilygomes130303@gmail.com</a>
 
            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px;color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/gmail.png" data-holder-rendered="truo d-block"true"> bs74648@gmail.com </a>

            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px ;color: white"><img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/gmail.png" data-holder-rendered="true">mleticianasc123@gmail.com </a>
            
            
</nav>


</div>
</nav>
<% } %>
    </body>
    
          <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
      <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

</html>
