<%-- 
    Document   : index
    Created on : 25/04/2020, 16:44:29
    Author     : emily,bia,Leticia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="./css/index.css" rel="stylesheet">
         <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
         <link  rel = "icon"  href = "./img/logo.ico"  type = "image / x-icon" >
        <title> KLB Sports - Home</title>
    </head>
    <body style="background-color:#ececec">
<nav class="navbar navbar-dark ">
  <a class="navbar-brand" href="#">
    <img src="./img/log.png" width="300" height="70" alt="">
  </a>
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Alternar de navegação">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="navbar-collapse collapse" id="navbarsExample01" style="">
    <ul class="navbar-nav mr-auto">
 
      
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Entrar</font></font></a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="cadastro.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cadastrar </font></font></a>
          <a class="dropdown-item" href="login.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Login </font></font></a>
        </div>
      </li>
    </ul>
  </div>
</nav>
  
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
      <div class="carousel-item active" id="topo">
        <img src="./img/KLB.png" class="d-block w-100" alt="Banner">
    </div>
  </div>
</div>
        <div class="container marketing">
       <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
            <img class="rounded-circle mx-auto d-block" src="./img/rapido.png" alt="Generic placeholder image" width="100" height="100">
          <h2 class="lead">Rápido</h2>
              </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
            <img class="rounded-circle mx-auto d-block" src="./img/rap.png" alt="Generic placeholder image" width="100" height="100">
            <h2 class="lead">Fácil</h2>
                </div><!-- /.col-lg-4 -->
          <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
            <img class="rounded-circle mx-auto d-block" src="./img/in.jpg" alt="Generic placeholder image" width="100" height="100">
            <h2 class="lead">Intuitivo</h2>
                </div><!-- /.col-lg-4 -->
        </div>
            
            <!-- Termina aqui as divs redondas -->
        
            <hr class="featurette-divider">
            
            <h3 class="titulo">
                Seja Bem-Vindo ao KLB Sports
            </h3> 
            <h4 class="corpo"> O KLB Sports foi desevolvido com o propósito de registrar seus jogos e  suas partidas.
                O mesmo executa de maneira fácil ,rápida e intuitiva com o propósito de facilitar o manuseio da aplicação.
                Segue  a baixo as instruções para uso da aplicação:
            </h4>
            <div class='row'>
                <div class="col-12"><img src="./img/inst.png" class="mx-auto d-block" style="margin-top: -58px;" width="350" height="200"/></div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 ">
            <h5 class="instru ">1. Para cadastrar os jogadores de determinados times é necessário se direcionar ao link "Cadastrar Jogador" que está
                localizado no DropDown com o nome Jogos.<br>
            <i class="instru "> OBS: Somente o técnico do time pode cadastrar seus jogadores. Para isso é preciso que ele se cadastre na aplicação.</i>
            <br> 
           
            <br>
            2. Para registrar os jogos e as partidas é necessário se direcionar o link "Registrar Jogo" que está localizado no DropDown com o nome Jogos. 
            Em seguida, se direcionar a pagina "Registrar Partidas" para o armazenamento dos pontos de cada partida.
           
            <br><i class="instru"> OBS: Somente o auxiliar técnico do time pode cadastrar partida e o tecnico o jogo. Para isso é preciso que ele se cadastre na aplicação.</i>

            </h5>
                    
                </div>
                <div class="col-lg-6 col-md-5 col-sm-5 col-xs-5" style="margin-top:-10px">
                   <img class="featurette-image img-fluid mx-auto" data-src="holder.js/400x350/auto" alt="400x350" style="width: 400px; height: 400px;" src="./img/menina.png" data-holder-rendered="true">   
                </div>
            </div>
            
            <hr class="featurette-divider">
            
            <div class="row" >
                 <div class="col-lg-6 col-md-5 col-sm-5 col-xs-5" style="margin-top:-10px">
                   <img class="featurette-image img-fluid mx-auto" data-src="holder.js/400x350/auto" alt="400x350" style="width: 400px; height: 400px;" src="./img/men.png" data-holder-rendered="true">   
                </div>
                <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7 ">
                    
                    <h5 class="instru "><strong>3. Cadastro:</strong>
                <br>
                <br>
                <p class="sub"> <strong>3.1. Técnico:</strong> Deverá preencher os campos obrigatorios: Nome  de Usuário, Email, Senha, Confirmar Senha, e juntamente à esses dados 
                    deverá informar a sua devida função.</p>
                        <br> 
                        <p class="sub" > <strong>3.2. Auxiliar:</strong> Deverá preencher os campos obrigatorios: Nome  de Usuário, Email, Senha, Confirmar Senha, e juntamente à esses dados 
                    deverá informar a sua devida função.</p>
                         
           
            <br><i class="instru"> OBS: Os cadastros apesar de serem feitos em uma mesma página, não interferem diretamente um na função do outro.</i>

         </h5>
                    
                </div>
               
            </div>
            <a href="#topo"><img class="mx-auto d-block" data-src="holder.js/400x350/auto" alt="400x350" style="width: 100px; height: 100px;" src="./img/Voltar ao Topo.png" data-holder-rendered="true">   
 </a>
            
        </div>
            
        <nav class="navbar navbar-expand-lg navbar-light ">
           
            <p style="color: white"> Contatos das Desenvolvedoras: </p>
            <a href="https://www.instagram.com/emily___gomes/" style="margin-left: 15px;margin-top: -13px; color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="./img/insta.png" data-holder-rendered="true"> @emily___gomes  </a>
 
            <a href="https://www.instagram.com/b_ssilva74/" style="margin-left: 15px;margin-top: -13px;color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="./img/insta.png" data-holder-rendered="truo d-block"true"> @b_ssilva74 </a>

           
            <a href="https://www.instagram.com/_lleticianasc/" style="margin-left: 15px;margin-top: -13px ;color: white"><img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="./img/insta.png" data-holder-rendered="true">@_lleticianasc </a>

            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px; color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="./img/gmail.png" data-holder-rendered="true"> emilygomes130303@gmail.com</a>
 
            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px;color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="./img/gmail.png" data-holder-rendered="truo d-block"true"> bs74648@gmail.com </a>

            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px ;color: white"><img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="./img/gmail.png" data-holder-rendered="true">mleticianasc123@gmail.com </a>
            
            
</nav>
    </body>
    
          <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
      <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

</html>
