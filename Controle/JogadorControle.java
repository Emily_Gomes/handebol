package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.JogadorModelo;

public class JogadorControle {
	public boolean inserirJogador(JogadorModelo jog){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO jogador(nome,numCamisa) VALUES (?,?);");
			ps.setString(1, jog.getNome());
			ps.setString(2, jog.getNumero());
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	
	public JogadorModelo selecionarPorId(int id) {
            JogadorModelo mo = new JogadorModelo();
        try {
        	Conexao con = new Conexao();
			String sql = "SELECT * FROM jogador WHERE id=?";
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                                mo.setId(rs.getInt("id"));
				mo.setNome(rs.getString("nome"));
				mo.setNumero(rs.getString("numCamisa"));
				System.out.print("erro");
            }
        } catch (SQLException e) {
    		System.out.println("Erro de SQL: "+e.getMessage());
        }catch(Exception e){
    		System.out.print("Error: "+ e.getMessage());
    		
    	}

        	return mo;
    	}
        public boolean update(JogadorModelo jog) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCon().prepareStatement("UPDATE jogador SET nome = ?, numCamisa = ?" + "WHERE id = ?;");
			ps.setInt(1, jog.getId());
			ps.setString(2, jog.getNome());
			ps.setString(3, jog.getNumero());
			
			if(!ps.execute()) {
				retorno = true;
			}
			
		 }catch (SQLException e) {
               System.out.println("Erro do banco: " + e.getMessage());
           }
		return retorno;
                
                
	}

         public boolean selecionarUm(JogadorModelo jog) {
            
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCon().prepareStatement("SELECT * jogador WHERE nome = ?;");
			ps.setString(1, jog.getNome());
                if(!ps.execute()) {
				retorno = true;
			}
			
		 }catch (SQLException e) {
               System.out.println("Erro do banco: " + e.getMessage());
           }
		return retorno;
                
                
	}
         
	public  boolean delete(JogadorModelo mo){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM jogador WHERE id = ? ;");
		ps.setInt(1, mo.getId());
		if(ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
        public ArrayList<JogadorModelo> selecionaTodos(){
	ArrayList<JogadorModelo> Info = null;
        try{
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM jogador;");
            ResultSet rs = ps.executeQuery();
            if(rs != null){
                Info = new ArrayList<JogadorModelo>();
            
            while(rs.next()) {
                 JogadorModelo part = new  JogadorModelo();
                
                part.setId(rs.getInt("id"));
                part.setNome(rs.getString("nome"));
                part.setNumero(rs.getString("numCamisa"));
             
                 Info.add(part);
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Info;
    }

	}

	
	

