<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="controle.Conexao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="css/jogos.css" rel="stylesheet">
         <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
         <link  rel = "icon"  href = "img/logo.ico"  type = "image / x-icon" >
        <title> KLB Sports - Dados</title>
    </head>
    <body style="background-color:#ececec">
         <%
		String email = (String) session.getAttribute("Ln1");
                String email2 = (String) session.getAttribute("Ln2");
		if(email2 != null){
			response.sendRedirect("cadjogador.jsp");
		}else if(email == null){
                    response.sendRedirect("login.jsp");
                    }else{
	%>
<nav class="navbar navbar-dark ">
  <a class="navbar-brand" href="index.jsp">
    <img src="img/log.png" width="300" height="70" alt="" href="index.jsp">
  </a>
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Alternar de navegação">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="navbar-collapse collapse" id="navbarsExample01" style="">
    <ul class="navbar-nav mr-auto">
 <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"> Jogos</font></a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="jogos.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Registrar Jogo </font></font></a>
    <a class="dropdown-item" href="cadjogador.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cadastrar Jogadores </font></font></a>
     <a class="dropdown-item" href="partida.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Registrar Dados da Partida </font></font></a>
 
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"> Dados</font></a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="TabPartida.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Vizualizar dados da Partida </font></font></a>
    <a class="dropdown-item" href="TabJogo.jsp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vizualizar dados do Jogo </font></font></a>
<a class="dropdown-item" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vizualizar Jogadores </font></font></a>

        </div>
      </li>
          
    
      
     <li class="nav-item ">
        <a class="nav-link text-white" href="sair.jsp" id="dropdown01"  aria-haspopup="true" aria-expanded="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sair</font></font></a>
          </li>
    </ul>
  </div>
</nav>
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
      <div class="carousel-item active" id="topo">
        <img src="img/dj.png" class="d-block w-100" alt="Banner">
    </div>
  </div>
</div>
        <div class="container marketing">
               <div class='row'>
                <div class="col-12"><img src="img/jc.png" class="mx-auto d-block" style="margin-top: -58px;" width="350" height="200"/></div>
            </div>
            
            
       
        <hr class="featurette-divider">
        <div class="table-responsive">
        
        <table border=1 class="table table-bordered table-hover">
          <thead class="thead-dark text-center">
            <tr>
                <th scope="col"> Id</th>
                <th scope="col"> Nome do Jogador Completo</th>
                <th scope="col"> Número da Camisa </th>
                
            </tr>
        </thead>
        <tbody class="text-center">
            
                <tr>
                    <%
                  Conexao con = new Conexao();
                  Statement ps = con.getCon().createStatement();
                  ResultSet rs = ps.executeQuery("SELECT * FROM jogador;");
                  while(rs.next()){
                      out.print("<tr>");
                      out.print("<td>" + rs.getString("id")+"</td>" );
                      out.print("<td>" + rs.getString("nome")+"</td>" );
                      out.print("<td>" + rs.getString("numCamisa")+"</td>" );
                   
                     out.print("</tr>");
                  }
              %>
                </tr>
           
        </tbody>
    </table>
        </div>
        </div>
  
                <nav class="navbar navbar-expand-lg navbar-light ">
           
            <p style="color: white"> Contatos das Desenvolvedoras: </p>
            <a href="https://www.instagram.com/emily___gomes/" style="margin-left: 15px;margin-top: -13px; color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/insta.png" data-holder-rendered="true"> @emily___gomes  </a>
 
            <a href="https://www.instagram.com/b_ssilva74/" style="margin-left: 15px;margin-top: -13px;color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/insta.png" data-holder-rendered="truo d-block"true"> @b_ssilva74 </a>

           
            <a href="https://www.instagram.com/_lleticianasc/" style="margin-left: 15px;margin-top: -13px ;color: white"><img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/insta.png" data-holder-rendered="true">@_lleticianasc </a>

            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px; color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/gmail.png" data-holder-rendered="true"> emilygomes130303@gmail.com</a>
 
            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px;color: white"> <img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/gmail.png" data-holder-rendered="truo d-block"true"> bs74648@gmail.com </a>

            <a href="https://www.google.com/intl/pt_pt/gmail/about/" style="margin-left: 15px;margin-top: -13px ;color: white"><img class="rounded float-left" data-src="holder.js/400x350/auto" alt="400x350" style="width: 18px; height: 18px;" src="img/gmail.png" data-holder-rendered="true">mleticianasc123@gmail.com </a>
            
            
</nav>
                <%
                }
                %>
    </body>
    
          <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
      <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

      
</html>

