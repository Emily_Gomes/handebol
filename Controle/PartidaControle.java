package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.PartidaModelo;

public class PartidaControle {
	public boolean inserirPartida(PartidaModelo part){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO popartida(gol,golAdv,falta,dataP) VALUES (?,?,?,?);");
			ps.setString(1, part.getGol());
			ps.setString(2, part.getGolAdv());
                        ps.setString(3, part.getFalta());
                        ps.setString(4, part.getDataP());
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	
 public  boolean delete(PartidaModelo user){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM popartida WHERE idPart = ? ;");
		ps.setInt(1, user.getIdPart());
		if(ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
        
      public ArrayList<PartidaModelo> selecionarTodos(){
		try{
			ArrayList<PartidaModelo> lista = null; 
			Conexao con = new Conexao();
			String sql = "SELECT * FROM popartida;";
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			if(ps.execute()){ 
				lista = new ArrayList<PartidaModelo>();
				ResultSet rs = ps.executeQuery();
				while(rs.next()){ 
					PartidaModelo part = new PartidaModelo();
					part.setIdPart(rs.getInt("idPart"));
					part.setGol(rs.getString("gol"));
					part.setGolAdv(rs.getString("golAdv"));
					part.setFalta(rs.getString("falta"));
                                        part.setDataP(rs.getString("dataP"));
					lista.add(part);
				}
				con.fecharConexao();
				return lista;

			}else{
				return lista;
			}
		}catch(SQLException e) {
				System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
			return null;
		}
		return null;

	}
      
      	public PartidaModelo selecionarPorId(int id) {
        PartidaModelo user = new PartidaModelo();
        try {
        	Conexao con = new Conexao();
			String sql = "SELECT * FROM popartida WHERE idPart =?";
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                    user.setIdPart(rs.getInt("idPart"));
                    user.setDataP(rs.getString("dataP"));
                    user.setGol(rs.getString("gol"));
                    user.setGolAdv(rs.getString("golAdv"));
                    user.setFalta(rs.getString("falta"));
                    
            }
        } catch (SQLException e) {
    		System.out.println("Erro de SQL: "+e.getMessage());
        }catch(Exception e){
    		System.out.print("Error: "+ e.getMessage());
    		return null;
    	}

        	return user;
    	}
	
	
	public boolean up(PartidaModelo user){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("UPDATE popartida SET dataP = ?, gol = ?, golAdv = ?, falta =?  WHERE idPart = ? ;");
		ps.setString(1, user.getDataP());
		ps.setString(2, user.getGol());
		ps.setString(3, user.getGolAdv());
                ps.setString(4, user.getFalta());
		ps.setInt(5, user.getIdPart());
		if(ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
	
	
}
//