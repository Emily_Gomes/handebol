package servlets;

import controle.AuxiliarControle;
import controle.TecnicoControle;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.TecnicoModelo;
import modelo.AuxiliarModelo;

@WebServlet("/ServLog")
public class ServLog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServLog() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			PrintWriter out = response.getWriter();
			if(request.getParameter("Lnivel").equals("1")) {
				String email = request.getParameter("Lnome");
				String senha = request.getParameter("Lsenha");
				TecnicoModelo tec = new TecnicoModelo();
				TecnicoControle use = new TecnicoControle();
			    tec.setEmail(email);
				tec.setSenha(senha);
				if(use.login(tec)){
                                        request.getSession().setAttribute("Ln1", email );
					response.sendRedirect("TabJogo.jsp");
				}else{
                                        request.getRequestDispatcher("login.jsp").forward(request, response);
				}
			}if(request.getParameter("Lnivel").equals("2")) {
				String email = request.getParameter("Lnome");
				String senha = request.getParameter("Lsenha");
				AuxiliarModelo tec = new AuxiliarModelo();
				AuxiliarControle use = new AuxiliarControle();
			    tec.setEmail(email);
				tec.setSenha(senha);
				if(use.login(tec)){
                                        request.getSession().setAttribute("Ln2", email);
					request.getRequestDispatcher("partida.jsp").forward(request, response);
				}else{
                                        request.getRequestDispatcher("login.jsp").forward(request, response);
                                  
				}		
			}
		}
	}


