package servlets;

import controle.JogoControle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.JogoModelo;


/**
 * Servlet implementation class ServUsuario
 */
@WebServlet("/ServUsuario")
public class ServUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        String metodo = request.getParameter("metodo");
        String destino = null;
        
        if (metodo.equalsIgnoreCase("deletar")){
        	JogoModelo user = new JogoModelo();
        	JogoControle userControl = new JogoControle();
        	String id = request.getParameter("idJogue");
        	user.setId(Integer.parseInt(id));
    		userControl.delete(user);
    		destino = "/TabJogo.jsp";
    		
        } else if (metodo.equalsIgnoreCase("editar")){
        	int id = Integer.parseInt(request.getParameter("idJogue"));
        	JogoControle userControl = new JogoControle();
        	JogoModelo user = userControl.selecionarPorId(id);
            request.setAttribute("upJogos", user);
            destino = "/upJogos.jsp";
            
        } else if (metodo.equalsIgnoreCase("selecionarTodos")){
        	JogoControle userControl = new JogoControle();
        	request.setAttribute("dadosJogo", userControl.selecionarTodos());
        	destino = "/dadosJogo.jsp";
        	
        } else {
            destino = "/login.jsp";
        }
        
        request.getRequestDispatcher(destino).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JogoModelo user = new JogoModelo();
		String id = request.getParameter("idJogue");
		user.setData(request.getParameter("data"));
		user.setHora(request.getParameter("hora"));
		user.setJuiz(request.getParameter("juiz"));
                user.setLocal(request.getParameter("local"));
                user.setSecretario(request.getParameter("secretario"));
                user.setTimeAdv(request.getParameter("timeAdv"));
		JogoControle control = new JogoControle();
		if(id.equalsIgnoreCase("")){
			request.getRequestDispatcher("/dadosp.jsp").forward(request, response);
        }
        else{
        	user.setId(Integer.parseInt(id));
            control.up(user);
            request.getRequestDispatcher("/dadosJogo.jsp").forward(request, response);
        }
		
		
		
		
		
	}

}
