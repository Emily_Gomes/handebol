package servlets;

import controle.JogoControle;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.JogoModelo;


@WebServlet("/ServJogo")
public class ServJogo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServJogo() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
			JogoModelo jog = new JogoModelo();
			JogoControle use = new JogoControle();
                        jog.setData(request.getParameter("data"));
                        jog.setHora(request.getParameter("hora"));
                        jog.setJuiz(request.getParameter("juiz"));
                        jog.setLocal(request.getParameter("local"));
                        jog.setSecretario(request.getParameter("secretario"));
                        jog.setTimeAdv(request.getParameter("timeAdv"));
		    	use.inserirJogo(jog);
                        response.sendRedirect("TabJogo.jsp");  
        }

}