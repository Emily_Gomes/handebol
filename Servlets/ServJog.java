package servlets;

import controle.JogoControle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.JogoModelo;


/**
 * Servlet implementation class ServJog
 */
@WebServlet("/ServJog")
public class ServJog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServJog() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        String metodo = request.getParameter("metodo");
        String destino = null;
        
        if (metodo.equalsIgnoreCase("deletar")){
        	JogoModelo jog = new JogoModelo();
        	JogoControle userControl = new JogoControle();
        	String id = request.getParameter("idJogue");
        	jog.setId(Integer.parseInt(id));
    		userControl.delete(jog);
    		destino = "/TabJogo.jsp";
    		
        } else if (metodo.equalsIgnoreCase("editar")){
        	int id = Integer.parseInt(request.getParameter("idJogue"));
        	JogoControle jogControl = new JogoControle();
        	JogoModelo jog = jogControl.selecionarPorId(id);
            request.setAttribute("upJogos", jog);
            destino = "/upJogos.jsp";
            
        } else if (metodo.equalsIgnoreCase("selecionarTodos")){
        	JogoControle userControl = new JogoControle();
        	request.setAttribute("dadosJogo", userControl.selecionarTodos());
        	destino = "/dadosJogo.jsp";
        	
        } else {
            destino = "/login.jsp";
        }
        
        request.getRequestDispatcher(destino).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JogoModelo jog = new JogoModelo();
		String id = request.getParameter("idJogue");
		jog.setData(request.getParameter("data"));
		jog.setHora(request.getParameter("hora"));
		jog.setJuiz(request.getParameter("juiz"));
                jog.setLocal(request.getParameter("local"));
                jog.setSecretario(request.getParameter("secretario"));
                jog.setTimeAdv(request.getParameter("timeAdv"));
		JogoControle control = new JogoControle();
		if(id.equalsIgnoreCase("")){
			request.getRequestDispatcher("/TabJogo.jsp").forward(request, response);
        }
        else{
        	jog.setId(Integer.parseInt(id));
            control.up(jog);
            request.getRequestDispatcher("/TabJogo.jsp").forward(request, response);
        }
		
		
		
		
		
	}

}
