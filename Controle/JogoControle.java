package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.JogoModelo;
import modelo.PartidaModelo;

public class JogoControle {
	public boolean inserirJogo(JogoModelo jog){
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO AgenJogo(hora,data,juiz,local,secretario,timeAdv) VALUES (?,?,?,?,?,?);");
			ps.setString(1, jog.getHora());
			ps.setString(2, jog.getData());
                        ps.setString(3, jog.getJuiz());
                        ps.setString(4, jog.getLocal());
                        ps.setString(5, jog.getSecretario());
                        ps.setString(6, jog.getTimeAdv());
                       
			if(!ps.execute()) {
				return true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no Banco: "+ e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Geral: "+ e.getMessage());
		}
	return retorno;	
	}
	
        public  boolean delete(JogoModelo user){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM agenjogo WHERE idJogue = ? ;");
		ps.setInt(1, user.getIdJogue());
		if(ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
        
      public ArrayList<JogoModelo> selecionarTodos(){
		try{
			ArrayList<JogoModelo> lista = null; 
			Conexao con = new Conexao();
			String sql = "SELECT * FROM agenjogo;";
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			if(ps.execute()){ 
				lista = new ArrayList<JogoModelo>();
				ResultSet rs = ps.executeQuery();
				while(rs.next()){ 
					JogoModelo user = new JogoModelo();
					user.setId(rs.getInt("idJogue"));
					user.setData(rs.getString("data"));
					user.setHora(rs.getString("hora"));
					user.setSecretario(rs.getString("secretario"));
                                        user.setJuiz(rs.getString("juiz"));
                                        user.setLocal(rs.getString("local"));
                                        user.setTimeAdv(rs.getString("timeAdv"));
					lista.add(user);
				}
				con.fecharConexao();
				return lista;

			}else{
				return lista;
			}
		}catch(SQLException e) {
				System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
			return null;
		}
		return null;

	}
      
      	public JogoModelo selecionarPorId(int id) {
        JogoModelo user = new JogoModelo();
        try {
        	Conexao con = new Conexao();
			String sql = "SELECT * FROM agenjogo WHERE idJogue =?";
			PreparedStatement ps = con.getCon().prepareStatement(sql);
			ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                    user.setId(rs.getInt("idJogue"));
                    user.setData(rs.getString("data"));
                    user.setHora(rs.getString("hora"));
                    user.setJuiz(rs.getString("juiz"));
                    user.setLocal(rs.getString("local"));
                    user.setSecretario(rs.getString("secretario"));
                    user.setTimeAdv(rs.getString("timeAdv"));
            }
        } catch (SQLException e) {
    		System.out.println("Erro de SQL: "+e.getMessage());
        }catch(Exception e){
    		System.out.print("Error: "+ e.getMessage());
    		return null;
    	}

        	return user;
    	}
	
	
	public boolean up(JogoModelo user){
		boolean retorno = false;
		try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("UPDATE agenjogo SET data = ?, hora = ?, juiz = ?, local = ?, secretario = ?, timeAdv = ?   WHERE idJogue = ? ;");
		ps.setString(1, user.getData());
		ps.setString(2, user.getHora());
		ps.setString(3, user.getJuiz());
                ps.setString(4, user.getLocal());
                ps.setString(5, user.getSecretario());
                ps.setString(6, user.getTimeAdv());
		ps.setInt(7, user.getIdJogue());
		if(ps.execute()) {
			retorno = true;
		}
		con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: "+e.getMessage());
		}catch(Exception e){
			System.out.print("Error: "+ e.getMessage());
		}
		return retorno;
		
	}
	
}
//