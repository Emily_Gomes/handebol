package servlets;

import controle.JogoControle;
import controle.PartidaControle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PartidaModelo;


/**
 * Servlet implementation class ServJog
 */
@WebServlet("/ServPart")
public class ServPart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServPart() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        String metodo = request.getParameter("metodo");
        String destino = null;
        
        if (metodo.equalsIgnoreCase("deletar")){
        	PartidaModelo part = new PartidaModelo();
        	PartidaControle parte = new PartidaControle();
        	String id = request.getParameter("idPart");
        	part.setIdPart(Integer.parseInt(id));
    		parte.delete(part);
    		destino = "/TabPartida.jsp";
    		
        } else if (metodo.equalsIgnoreCase("editar")){
        	int id = Integer.parseInt(request.getParameter("idPart"));
        	PartidaControle parte = new PartidaControle();
        	PartidaModelo part = parte.selecionarPorId(id);
            request.setAttribute("upPartida", part);
            destino = "/upPartida.jsp";
            
        } else if (metodo.equalsIgnoreCase("selecionarTodos")){
        	PartidaControle parte = new PartidaControle();
        	request.setAttribute("dadosp", parte.selecionarTodos());
        	destino = "/dadosp.jsp";
        	
        } else {
            destino = "/dadosp.jsp";
        }
        
        request.getRequestDispatcher(destino).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PartidaModelo part = new PartidaModelo();
		String id = request.getParameter("idPart");
		part.setDataP(request.getParameter("dataP"));
		part.setGol(request.getParameter("gol"));
		part.setGolAdv(request.getParameter("golAdv"));
                part.setFalta(request.getParameter("falta"));
		PartidaControle control = new PartidaControle();
		if(id.equalsIgnoreCase("")){
			request.getRequestDispatcher("/TabPartida.jsp").forward(request, response);
        }
        else{
        	part.setIdPart(Integer.parseInt(id));
            control.up(part);
            request.getRequestDispatcher("/TabPartida.jsp").forward(request, response);
        }
		
		
		
		
		
	}

}
