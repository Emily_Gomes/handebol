/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Biscoito
 */
public class JogoModelo {
    private int idJogue;
    private String local;
    private String data;
    private String hora;
    private String timeAdv;
    private String juiz;
    private String secretario;

    public int getIdJogue() {
        return idJogue;
    }

    public void setId(int idJogue) {
        this.idJogue = idJogue;
    }

    
    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getTimeAdv() {
        return timeAdv;
    }

    public void setTimeAdv(String timeAdv) {
        this.timeAdv = timeAdv;
    }

    public String getJuiz() {
        return juiz;
    }

    public void setJuiz(String juiz) {
        this.juiz = juiz;
    }

    public String getSecretario() {
        return secretario;
    }

    public void setSecretario(String secretario) {
        this.secretario = secretario;
    }
    
    
}
