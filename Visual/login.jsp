<%-- 
    Document   : login
    Created on : 28/04/2020, 22:58:32
    Author     : emily
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/login.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <link  rel = "icon"  href = "img/logo.ico"  type = "image / x-icon" >
        <title>Login</title>
    </head>
    <body style="background-image: url('img/r.jpg');">
        <div class="container">
	<div class="d-flex justify-content-center h-100">
        <div class="card">
        <div class="card-header">
            <h3>Login</h3>
	</div>
	<div class="card-body">
	<form action="ServLog" method="post">
             <label for="username" class="text">Usuário</label>
	<div class="input-group form-group">
            
	<div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user"></i></span>
	</div>
           
            <input type="text" class="form-control" name="Lnome" placeholder="Digite o usuário">
						
	</div>
              <label for="senha" class="text">Senha</label>
        <div class="input-group form-group">
	<div class="input-group-prepend">
	<span class="input-group-text"><i class="fas fa-key"></i></span>
	</div>
            <input type="password" class="form-control" name="Lsenha" placeholder="Digite sua senha">
	</div>
             <div class="row align-items-center remember">
             <label class="label-input">
                        <i class="far fa-user icon-modify"></i>
                        <select name="Lnivel" id="Lnivel" required>
                        	<option value="" disabled selected>Nível de Acesso</option>
                        	<option value="1">Tecnico</option>
                        	<option value="2">Auxiliar Técnico</option>
                        </select>
                    </label>
	</div>
              
	<div class="form-group">
	<input type="submit" value="Login" href="#" class="btn btn-light">
	</div>
        </form>
            <a href='cadastro.jsp' id='btn btn-light' ><button class="blogin">Não possui cadastro?</button></a>
   <a href='index.jsp' id='btn btn-light'><button class="ini">Menu</button></a>
 
        </div>
	</div>
	</div>
        </div>
        
    </body >
</html>

