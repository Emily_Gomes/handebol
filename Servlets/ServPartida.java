package servlets;

import controle.PartidaControle;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.PartidaModelo;


@WebServlet("/ServPartida")
public class ServPartida extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServPartida() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
			PartidaModelo part = new PartidaModelo();
			PartidaControle use = new PartidaControle();
                        part.setGol(request.getParameter("gol"));
                        part.setGolAdv(request.getParameter("golAdv"));
                        part.setFalta(request.getParameter("falta"));
                        part.setDataP(request.getParameter("dataP"));
		    	use.inserirPartida(part);
                        response.sendRedirect("TabPartida.jsp");  
        }

}