package servlets;

import controle.JogadorControle;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.JogadorModelo;


@WebServlet("/ServCadJog")
public class ServCadJog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServCadJog() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
			String nome = request.getParameter("nome");
			JogadorModelo jog = new JogadorModelo();
			JogadorControle use = new JogadorControle();
                        jog.setNome(nome);
		    if(use.selecionarUm(jog)){
                        response.sendRedirect("cadjogador.jsp");
		    }else{
                                jog.setNome(request.getParameter("nome"));
				jog.setNumero(request.getParameter("numero"));
				
		    	use.inserirJogador(jog);
                        response.sendRedirect("cadjogador.jsp");
			}
        }
}
