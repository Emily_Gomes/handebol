<%-- 
    Document   : cadastro
    Created on : 28/04/2020, 23:02:59
    Author     : emily
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="css/cadastro.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <link  rel = "icon"  href = "img/logo.ico"  type = "image / x-icon" >
       
        <title>Cadastro</title>
    </head>
    <body style="background-image: url('img/r.jpg');">
        <div class="container">
	<div class="d-flex justify-content-center h-100">
        <div class="card">
        <div class="card-header">
            <h3>Cadastre-se</h3>
	</div>
	<div class="card-body">
	<form action="ServCad" method="post">
             <label for="username" class="text">Usuário</label>
	<div class="input-group form-group">
            
	<div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user"></i></span>
	</div>
           
            <input type="text" class="form-control" name="nome" placeholder="Digite seu úsuario">
						
	</div>
              <label for="Email" class="text">Email</label>
        <div class="input-group form-group">
	<div class="input-group-prepend">
	<span class="input-group-text"><i class="fas fa-key"></i></span>
	</div>
            <input type="text" class="form-control" placeholder="Email" name="email">
	</div>
              <label for="senha" class="text">Senha</label>
	<div class="input-group form-group">
	<div class="input-group-prepend">
	<span class="input-group-text"><i class="fas fa-key"></i></span>
	</div>
            <input type="password" class="form-control" name="senha" placeholder="Digite sua senha">
	</div>
              
        <div class="row align-items-center remember">
           <label class="label-input">
                        <i class="far fa-user icon-modify"></i>
                        <select name="nivel" id="nivel" required>
                        	<option value="" disabled selected>Nível de Acesso</option>
                        	<option value="1">Técnico</option>
                        	<option value="2">Auxiliar Técnico</option>
                        </select>
                    </label>
                    
	</div>
	
	<div class="form-group">
	<input type="submit" value="Cadastrar" href="cadastro" class="btn btn-light">
	</div>
        
        </form>
            <a href='login.jsp' id='btn btn-light' ><button class="bcad">Já possui cadastro?</button></a>
   <a href='index.jsp' id='btn btn-light'><button class="bcad">Voltar ao menu</button></a>
 
        </div>
	</div>
	</div>
</div>
       
    </body>
</html>


